#include "paths.h"
#define MAX 7
using namespace std;
int main(){
    path_record arr[MAX];
    path_record p;
    int     fh; 
    int i = 0;

    //CONVERT FROM DB FILE AND PUT INTO AN ARRAY
    if ((fh = open ("sample.db", O_RDONLY)) <0) {
        fprintf (stderr, "sample.db Open Failed\n");
        perror ("Paths");
        exit (1);
    }   
    while (read (fh, &p, sizeof (p) > 0)){
        strcpy(arr[i].City1, p.City1);
        strcpy(arr[i].City2, p.City2);
        arr[i].distance = p.distance;
        arr[i].travel_time = p.travel_time; 
        i++;
    }   
    close (fh);
    //PRINT STATEMENT FOR TESTING THE VALUES OF ARRAY
       for (int j = 0; j < MAX; j++){
       printf("****RECORD %d****\n", (j+1));
       printf("City 1 is: %s\n", arr[j].City1);
       printf("City 2 is: %s\n", arr[j].City2);
       printf("Distance is: %d\n", arr[j].distance);
       printf("Time is: %d\n\n\n", arr[j].travel_time);
       }   

    //NEW CODE
    string city1("");
    string city2("");
    for (int j = 0; j <= MAX; j++){
        if (city1 == arr[j].City1){
            if (city2 == arr[j].City2)
                cout << city1 << " is adj to " << city2 << endl;
            else
                cout << city1 << " is NOT adj to " << city2 << endl;
        }   
        else{
            cout << "no matches???" << endl;
        }
    }
}