#include <cstring> 
#include <unistd.h>
#include <errno.h>
#include <string>
#include <iostream>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
typedef struct {
    char City1[5];
    char City2[5];
    int distance, travel_time;
} path_record;